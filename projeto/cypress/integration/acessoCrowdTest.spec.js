///<reference types="Cypress"/>

//Import de Page
import loginPage from "/Users/laedyceci/Desktop/ProjetoCy/projeto/cypress/support/page/LoginPage.js"

//teste de um mesmo grupo podem ficar dentro de um describe
describe('loginCrowdTest', ()=>{
    
    //uso o beforeEach p este iniciar antes de cada teste (it)
    beforeEach(()=>{
        cy.visit(Cypress.config('url'))  
    })

    it('RealizarLoginSucesso', ()=>{
        //Atributos
        var email = Cypress.config('email')
        var senha = Cypress.config('senha')
        var textoEsperado = 'Testar'

        //Instanciando os métodos da Page
        loginPage.preencherEmail(email)
        loginPage.preencherSenha(senha)
        loginPage.clicarLogar()

        //Validacao
        loginPage.validarLogin(textoEsperado)
    })

    //DATA-DRIVEN - ao usar dataDriven nao posso usar funcao =>
    const senhas = [1234, 1235, 8965]
    senhas.forEach(senha=>{
        it(`RealizarLoginInvalido - DataDriven ${senha}`, function(){
            //Atributos
            var email = Cypress.config('email')
            var msgErro = 'E-mail ou senha inválidos.'
            
            //Instanciando os métodos da Page
            loginPage.preencherEmail(email)
            loginPage.preencherSenha(senha)
            loginPage.clicarLogar()
    
            //Validacao
            loginPage.ValidarLoginInvalido(msgErro)
        })
    })
})
